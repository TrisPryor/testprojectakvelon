package animal;

public enum AnimalType {
    DOG("Bark"),
    SHEEP("Be-e-e");

    String voice;

    AnimalType(String voice) {
        this.voice = voice;
    }
}
