package animal;

public class Animal {
    private Coordinate coordinate = new Coordinate();
    private AnimalType type;

    public Animal(AnimalType type) {
        this.type = type;
    }

    public String getVoice() {
        return type.voice;
    }

    public AnimalType getType() {
        return type;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void move(int x, int y) {
        coordinate.setX(x);
        coordinate.setY(y);
    }
}
