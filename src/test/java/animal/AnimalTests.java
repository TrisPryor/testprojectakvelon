package animal;

import org.junit.jupiter.api.Test;

import static animal.AnimalType.DOG;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AnimalTests {

    @Test
    public void verifyDogVoice() {
        final Animal dog = new Animal(DOG);
        assertEquals(dog.getVoice(), DOG.voice);
    }
}
